package gatlingdemo

import io.gatling.javaapi.core.CoreDsl.*
import io.gatling.javaapi.core.Simulation
import io.gatling.javaapi.http.HttpDsl.*
import java.util.*

@Suppress("unused")
class LargeSimulation : Simulation() {

    val newPostDataGenerator = generateSequence(0) { it + 1 }
        .map {
            mapOf(
                "id" to it,
                "title" to "Generated Title $it",
                "body" to "Generated Body $it"
            )
        }

    val getPosts = exec(
        http("get Posts") // Name of the request
            .get("https://jsonplaceholder.typicode.com/posts")
            .check(
                status().`is`(200),
            )
    )

    val createPost = exec(feed(newPostDataGenerator.iterator()))
        .exec(
            http("create Post") // Name of the request
                .post("https://jsonplaceholder.typicode.com/posts")
                .body(ElFileBody("createPostRequest.json"))
                .check(
                    status().`is`(201),
                )
        )

    val updatePost = exec(
        http("get Post")
            .get("https://jsonplaceholder.typicode.com/posts/1")
            .check(
                status().`is`(200),
                substring("reprehenderit"),
                jsonPath("$.title").saveAs("title")
            )
    )
        .exitHereIfFailed()
        .exec { s -> s.set("newTitle", s.get<String>("title")!!.uppercase(Locale.getDefault())) }
        .exec(
            http("patch post")
                .patch("https://jsonplaceholder.typicode.com/posts/1")
                .body(ElFileBody("patchPost.json"))
                .check(
                    status().`is`(200)
                )
        )

    val usersScenario = scenario("Default Scenario").exec(getPosts, createPost, updatePost)
    val createScenario = scenario("Creator").exec(createPost)

    init {
        setUp(
            usersScenario.injectOpen(
                constantUsersPerSec(1.0).during(3),
            ),
            createScenario.injectOpen(
                rampUsersPerSec(1.0).to(3.0).during(3),
            ),
        ).protocols(
            http.authorizationHeader("Bearer thisCouldBeYourAuthToken").enableHttp2()
        )
    }
}