package gatlingdemo

import io.gatling.javaapi.core.CoreDsl.*
import io.gatling.javaapi.core.Simulation
import io.gatling.javaapi.http.HttpDsl.*

@Suppress("unused")
class MinimalSimulation : Simulation() {

    val getPosts = exec(
        http("getPosts") // Name of the request
            .get("https://jsonplaceholder.typicode.com/posts")
            .check(
                status().`is`(200),
            )
    )

    val usersScenario = scenario("First Scenario").exec(getPosts)

    init {
        setUp(
            usersScenario.injectOpen(
                constantUsersPerSec(1.0).during(1),
            ),
        )
    }
}