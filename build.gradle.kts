plugins {
    kotlin("jvm") version "1.9.23"
    id("idea")
    id("io.gatling.gradle") version "3.10.5"
}

gatling {
    logLevel = "INFO" // logback root level
    logHttp = io.gatling.gradle.LogHttp.ALL
}

repositories {
    mavenCentral()
}

idea {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

kotlin {
    jvmToolchain(17)
}